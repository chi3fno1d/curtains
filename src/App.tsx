import React from 'react';
import './App.module.scss';
import AppRouter from './components/AppRouter';

const App: React.FC = () => {
  return <AppRouter />;
};

export default App;

import React from 'react';
import CurtainsBlock from '../components/curtainsBlock/CurtainsBlock';

const Home: React.FC = () => {
  return (
    <div>
      <CurtainsBlock />
    </div>
  );
};

export default Home;
